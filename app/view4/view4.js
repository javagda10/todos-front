'use strict';

angular.module('myApp.view4', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view4', {
            templateUrl: 'view4/view4.html',
            controller: 'View4Ctrl'
        });
    }])
    .controller('View4Ctrl', ['$http', function ($http) {
        var self = this; // self to obiket View2Ctrl

        this.zaladujListeUzytkownikow = function () {
            $http.get('http://localhost:8080/listUsers')
                .then(
                    // jeśli nie bedzie bledu
                    function (dane) {
                        console.log('Wszystko bangla!');
                        console.log(dane);

                        // dane.data -> to body ramki http
                        self.dane = dane.data;
                    },
                    // jesli bedzie blad
                    function (blad) {
                        console.log('Ej, jest błąd!');
                    });
        };

        this.zaladujListeUzytkownikow();
    }]);