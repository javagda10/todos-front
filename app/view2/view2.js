'use strict';

angular.module('myApp.view2', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view2', {
            templateUrl: 'view2/view2.html',
            controller: 'View2Ctrl'
        });
    }])

    .controller('View2Ctrl', ['$http', function ($http) {
        var self = this; // self to obiket View2Ctrl

        this.zaladujZadania = function () {
            $http.get("http://localhost:8080/api/listTasks")
                .then(
                    function (dane) {
                        //
                        console.log(dane);
                        self.zadania = dane.data;
                    },
                    function (error) {
                        console.log("error - " + error);
                    }
                );
        };

        this.changeTaskState = function (taskId) {
            $http.post("http://localhost:8080/api/changeState/" + taskId, {'set_state': true})
                .then(
                    function (dane) {
                        console.log("Udalo sie: " + dane);
                        self.zaladujZadania();
                    },
                    function (error) {
                        console.log("error: " + error);
                    });
        };

        this.zaladujZadania();

    }]);