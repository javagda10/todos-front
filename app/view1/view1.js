'use strict';

angular.module('myApp.view1', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view1', {
            templateUrl: 'view1/view1.html',
            controller: 'View1Ctrl'
        });
    }])
    .controller('View1Ctrl', ['$http', '$window', function ($http, $window) {
        var self = this; // self to obiket View2Ctrl

        this.zaladujListeUzytkownikow = function () {
            $http.get('http://localhost:8080/api/listUsers')
                .then(
                    // jeśli nie bedzie bledu
                    function (dane) {
                        console.log('Wszystko bangla!');
                        console.log(dane);

                        // dane.data -> to body ramki http
                        self.dane = dane.data;
                    },
                    // jesli bedzie blad
                    function (blad) {
                        console.log('Ej, jest błąd!');
                    });
        };

        this.editUsers = function (some_user) {
            console.log('Redirect');
            $window.location = '#!/view5?edited_user_id=' + some_user;
        };

        this.zaladujListeUzytkownikow();
    }]);