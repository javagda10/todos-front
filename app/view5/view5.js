'use strict';

angular.module('myApp.view5', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view5', {
            templateUrl: 'view5/view5.html',
            controller: 'View5Ctrl'
        });
    }])
    .controller('View5Ctrl', ['$http', '$routeParams', '$window',
        function ($http, $routeParams, $window) {
            var self = this; // self to obiket View2Ctrl

            // $window.location= "#!/view5?edited_user_id=" + user_id;

            console.log("Parametr edited_user_id=" + $routeParams.edited_user_id);

            self.edited_user_id = $routeParams.edited_user_id;

            this.ramka_edycji = {
                'edited_name': '',
                'edited_surname': '',
                'edited_address': ''
            };

            this.wyslij = function () {
                $http.post("http://localhost:8080/editUser/" + self.edited_user_id, self.ramka_edycji)
                    .then(
                        function (dane) {
                            console.log("udalo sie");
                            $window.location = "#!/view1";
                        },
                        function (error) {
                            console.log("nie udalo sie " + error);
                        });
            };

            this.zaladujDaneOUzytkowniku = function () {
                $http.get("http://localhost:8080/getUserData/" + self.edited_user_id)
                    .then(
                        function (dane) {
                            console.log(dane.data);
                            self.dane_uzytkownika = dane.data;

                            self.ramka_edycji.edited_address = self.dane_uzytkownika.address;
                            self.ramka_edycji.edited_name = self.dane_uzytkownika.name;
                            self.ramka_edycji.edited_surname = self.dane_uzytkownika.surname;
                        },
                        function (error) {
                            console.log("Error: " + error);
                        });
            };

            this.zaladujDaneOUzytkowniku();
        }]);