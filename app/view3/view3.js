'use strict';

angular.module('myApp.view3', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view3', {
            templateUrl: 'view3/view3.html',
            controller: 'View3Ctrl'
        });
    }])

    .controller('View3Ctrl', ['$http', '$window', function ($http, $window) {
        var self = this;

        self.objectToSend = {
            'add_title': '',
            'add_description': ''
        };

        this.stworzNoweZadanie = function () {
            $http.post("http://localhost:8080/addTask", self.objectToSend)
                .then(
                    function (dane) {
                        console.log("Dane: " + dane);
                        $window.location = "#!/view2";
                    },
                    function (error) {
                        console.log("Error: " + error);
                    });
        };

    }]);